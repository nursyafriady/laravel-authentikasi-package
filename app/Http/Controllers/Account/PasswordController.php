<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function edit()
    {
        return view('password.edit');
    }

    public function update()
    {
        // return view('password.edit');
        // dd('change password');
        request()->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'old_password' => 'required',
        ]);

        $currentPassword = auth()->user()->password;
        $old_password = request('old_password');

        if(Hash::check($old_password, $currentPassword)) {
            auth()->user()->update([
              'password' => bcrypt(request('password')),
            ]);
            return back()->with(['success' => 'Password berhasil diganti']);
        } else {
            return back()->withErrors(['old_password' => 'Password Lama Salah']);
        }

    }
}
