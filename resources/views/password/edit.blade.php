@extends('layouts.app', ['title' =>'Change Password'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Password') }}</div>

                <div class="card-body">
                    @if(session('success'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('success') }}  
                    </div>
                    @endif

                   <form action="{{ route('password-edit') }}" method="post">
                        @method('PATCH')
                        @csrf

                        <div class="form-group">
                            <label for="old_password">Change Password</label>
                            <input type="password" name="old_password" id="old_password" 
                                   class="form-control @error('old_password') is-invalid @enderror">
                            @error('old_password') 
                                <div class="alert alert-danger">{{ $message }}</div>                            
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                            @error('password') 
                                <div class="alert alert-danger">{{ $message }}</div>                            
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Konfirmasi Password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                        </div>

                        <button type="submit" class="btn btn-primary">
                            Ubah Password
                        </button>
                   </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
